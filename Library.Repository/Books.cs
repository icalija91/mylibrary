﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repository
{
    public class Books : Base
    {
        public string Name { get; set; }

        public string Author { get; set; }

        public int Year { get; set; }

        public string Category { get; set; }

        public int Rating { get; set; }

        public string Description { get; set; }


        public LibraryUsers LibraryUser { get; set; }

    }
}
