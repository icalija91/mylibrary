﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repository.Repositories.Interfaces
{
    public interface IBaseRepository<TEntity>
    {
        TEntity ReadOne(int Id);

        List<TEntity> ReadAll();

        int? Insert(TEntity model);

        int? Delete(TEntity model);

        int? Update(TEntity model);
    }
}
