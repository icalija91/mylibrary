﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repository.Repositories.Interfaces
{
    public interface IBooksRepository : IBaseRepository <Books>
    {
        List<Books> ReadAllByUserId(string userId, string search);


    }
}
