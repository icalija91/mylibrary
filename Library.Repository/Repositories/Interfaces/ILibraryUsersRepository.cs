﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Repository.Repositories.Interfaces;


namespace Library.Repository.Repositories.Interfaces
{
    public interface ILibraryUsersRepository : IBaseRepository<LibraryUsers>
    {

    }
}
