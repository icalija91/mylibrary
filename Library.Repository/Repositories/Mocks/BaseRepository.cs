﻿using Dapper;
using Library.Repository.Repositories.Interfaces;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repository.Repositories.Mocks
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : Base
    {
        private readonly ConnectionStrings connectionStrings;

        public BaseRepository(IOptions<ConnectionStrings> connectionStrings)
        {
            this.connectionStrings = connectionStrings.Value;
        }

        public TEntity ReadOne(int Id)
        {
            using (var db = new SqlConnection(connectionStrings.DefaultConnection))
            {
                var result = db.Get<TEntity>(Id);

                return result;
            }
        }

        public List<TEntity> ReadAll()
        {
            using (var db = new SqlConnection(connectionStrings.DefaultConnection))
            {
                var result = db.GetList<TEntity>("where userid= userId");

                return result.ToList();
            }
        }

        public int? Insert(TEntity model)
        {
            model.DateCreated = DateTime.UtcNow;
            model.DateUpdated = DateTime.UtcNow;

            using (var db = new SqlConnection(connectionStrings.DefaultConnection))
            {
                var result = db.Insert<TEntity>(model);

                return result;
            }
        }

        public int? Update(TEntity model)
        {
            model.DateUpdated = DateTime.UtcNow;

            using (var db = new SqlConnection(connectionStrings.DefaultConnection))
            {
                var result = db.Update<TEntity>(model);

                return result;
            }
        }

        public int? Delete(TEntity model)
        {
            using (var db = new SqlConnection(connectionStrings.DefaultConnection))
            {
                var result = db.Delete<TEntity>(model);

                return result;
            }
        }
    }

}
