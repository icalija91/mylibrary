﻿using Dapper;
using Library.Repository.Repositories.Interfaces;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repository.Repositories.Mocks
{
    public class BooksRepository : BaseRepository<Books>, IBooksRepository
    {
        private readonly ConnectionStrings connectionStrings;

        public BooksRepository(IOptions<ConnectionStrings> connectionStrings) : base(connectionStrings)
        {
            this.connectionStrings = connectionStrings.Value;
        }

        /// <summary>
        /// Read all books for loged in user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<Books> ReadAllByUserId(string userId, string search)
        {
            if (string.IsNullOrEmpty(search)) search = "";

            using (var db = new SqlConnection(connectionStrings.DefaultConnection))
            {
                string sql = "SELECT b.*, lu.* FROM Books b " +
                    "INNER JOIN LibraryUsers lu ON b.Id = lu.BooksId " +
                    "WHERE lu.UserId = '" + userId + "' AND UPPER(b.Name) like '%" + search.ToUpper() + "%';";
                return db.Query<Books, LibraryUsers, Books>(sql, (b, lu) => { b.LibraryUser = lu; return b; }, splitOn: "Id", commandType: System.Data.CommandType.Text).ToList();
            }
        }
    }
}
