﻿using Library.Repository.Repositories.Interfaces;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repository.Repositories.Mocks
{
    public class LibraryUsersRepository : BaseRepository<LibraryUsers>, ILibraryUsersRepository
    {
        public LibraryUsersRepository(IOptions<ConnectionStrings> connectionStrings) : base(connectionStrings)
        {
        }
    }
}
