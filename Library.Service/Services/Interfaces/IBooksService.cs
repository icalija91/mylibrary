﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Service.Services.Interfaces
{
    public interface IBooksService
    {
        bool Create(Repository.Books data, string logedInUserId);

        List<Repository.Books> ReadAllByUserId(string userId, string search);

        int? Delete(int Id);

        Repository.Books ReadOne(int Id);

        int? Edit(Repository.Books data);


    }
}
