﻿using Library.Repository.Repositories.Interfaces;
using Library.Service.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Service.Services.Mocks
{
    public class BooksService : IBooksService
    {
        private readonly IBooksRepository booksRepository;
        private readonly ILibraryUsersRepository libraryUsersRepository;

        public BooksService(IBooksRepository booksRepository, ILibraryUsersRepository libraryUsersRepository)
        {
            this.booksRepository = booksRepository;
            this.libraryUsersRepository = libraryUsersRepository;
        }

        public bool Create(Repository.Books data, string logedInUserId)
        {
            try
            {
                data.UserCreated = logedInUserId;
                data.UserUpdated = logedInUserId;

                var lastCredentialsAddedId = booksRepository.Insert(data);

                Repository.LibraryUsers libraryUsersData = new Repository.LibraryUsers();

                libraryUsersData.UserCreated = logedInUserId;
                libraryUsersData.UserUpdated = logedInUserId;

                libraryUsersData.BooksId = (int)lastCredentialsAddedId;
                libraryUsersData.UserId = logedInUserId;

                var lastUserCredentialsAddedId = libraryUsersRepository.Insert(libraryUsersData);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<Repository.Books> ReadAllByUserId(string userId, string search)
        {
            return booksRepository.ReadAllByUserId(userId, search);
        }

        public int? Delete(int Id)
        {
            return booksRepository.Delete(new Repository.Books { Id = Id });
        }

        public Repository.Books ReadOne(int Id)
        {
            return booksRepository.ReadOne(Id);
        }

        public int? Edit(Repository.Books data)
        {
            data.DateUpdated = DateTime.UtcNow;

            var oldData = booksRepository.ReadOne(data.Id);
            data.DateCreated = oldData.DateCreated;
            data.UserCreated = oldData.UserCreated;
            data.UserUpdated = oldData.UserUpdated;


            return booksRepository.Update(data);
        }
    }
}
