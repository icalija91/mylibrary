﻿using Library.Web.Areas.Account.Models;
using Library.Web.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Web.Areas.Account.Controllers
{
    [Area("Account")]
    [Route("account/[controller]/[action]")]
    public class IdentityController : Controller
    {

        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;

        public IdentityController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }


        public IActionResult Index()
        {
            return View();
        }

        #region HttpGet methods

        public IActionResult Registration() => View();

        public IActionResult Login() => View();

        public IActionResult Profile()
        {
            var user = userManager.GetUserAsync(User).Result;

            return View(user);
        }

        public async Task<IActionResult> Logout(string returnUrl = null)
        {
            await signInManager.SignOutAsync();

            if (returnUrl != null)
            {
                return LocalRedirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Login", "Identity");
            }
        }

        #endregion HttpGet methods

        #region HttpPost methods

        [HttpPost]
        public async Task<IActionResult> Profile(ApplicationUser model)
        {
            var user = userManager.GetUserAsync(User).Result;
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.PhoneNumber = model.PhoneNumber;
            user.Address = model.Address;
            user.City = model.City;
            user.Country = model.Country;
            user.PostalCode = model.PostalCode;
            user.Description = model.Description;
            var result = await userManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                TempData["Response"] = true;
                TempData["ResponseMessage"] = "Successfuly edited profile";
            }
            else
            {
                TempData["Response"] = false;

                foreach (var item in result.Errors)
                {
                    TempData["ResponseMessage"] = item.Description;
                }
            }

            return View(user);
        }

        [HttpPost]
        public async Task<IActionResult> RegistrationAsync(RegistrationModel model)
        {

            if (!ModelState.IsValid)
            {
                TempData["Response"] = false;
                return View();

            }
            var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
            var result = await userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                TempData["Response"] = true;
                TempData["ResponseMessage"] = "Successfuly registered";
            }
            else
            {
                TempData["Response"] = false;

                foreach (var item in result.Errors)
                {
                    TempData["ResponseMessage"] = item.Description;
                }
            }

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginModel model)
        {
            var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);

            if (result.Succeeded)
            {
                return RedirectToAction("Index", "Home", new { Area = "" });
            }
            else
            {
                TempData["Response"] = false;
                TempData["ResponseMessage"] = "Login failed";
            }

            return View();
        }

        #endregion HttpPost methods

    }
}