﻿using AutoMapper;
using Library.Service.Services.Interfaces;
using Library.Web.Areas.Books.Models;
using Library.Web.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Web.Areas.Books.Controllers
{
    [Authorize]
    [Area("Books")]
    [Route("books/[controller]/[action]")]
    public class HomeController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IBooksService booksService;
        private readonly IMapper mapper;

        public HomeController(IBooksService booksService, UserManager<ApplicationUser> userManager, IMapper mapper)
        {
            this.booksService = booksService;
            this.userManager = userManager;
            this.mapper = mapper;
        }

        public async Task<IActionResult> IndexAsync(string search = "")
        {
            var user = await userManager.GetUserAsync(User);

            ViewBag.Password = "Test123";
            ViewBag.Email = user.Email;

            return View(new List<BooksModel>());
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(BooksModel model)
        {
            if (!ModelState.IsValid)
            {
                TempData["Response"] = false;
                TempData["ResponseMessage"] = "There are validation errors";

                return View(model);
            }

            var result = booksService.Create(mapper.Map<Repository.Books>(model), userManager.GetUserAsync(User).Result.Id);

            TempData["Response"] = true;
            TempData["ResponseMessage"] = "Successfuly created";
            return Redirect("~/Home/Index");
        }

        [HttpPost]
        public IActionResult Delete(int Id)
        {
            try
            {
                var result = booksService.Delete(Id);
                return Content("Book successfuly deleted");
            }
            catch (Exception e)
            {
                return Content("An error occured during removal");
            }

        }

        public IActionResult Edit(int booksId)
        {
            var data = booksService.ReadOne(booksId);

            return View(mapper.Map<BooksModel>(data));
        }

        [HttpPost]
        public IActionResult Edit(BooksModel model)
        {
            if (!ModelState.IsValid)
            {
                TempData["Response"] = false;
                TempData["ResponseMessage"] = "There are validation errors";

                return View(model);
            }

            var result = booksService.Edit(mapper.Map<Repository.Books>(model));

            TempData["Response"] = false;
            TempData["ResponseMessage"] = "Successfully edited";

            return Redirect("~/Home/Index");
        }

        public IActionResult Cancel()
        {
            return Redirect("~/Home/Index");
        }
    }
}
