﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Web.Areas.Books.Models
{
    public class BooksModel : BaseModel
    {

        [Required]
        [Display(Name = "Name", Prompt = "Name")]
        public string Name { get; set; }

        [Display(Name = "Author", Prompt = "Author")]
        public string Author { get; set; }

        [Display(Name = "Year", Prompt = "Year")]
        public int Year { get; set; }

        [Display(Name = "Category", Prompt = "Category")]
        public string Category { get; set; }

        
        [Display(Name = "Rating", Prompt = "Rating")]
        public int Rating { get; set; }

        [Display(Name = "Description", Prompt = "Description")]
        public string Description { get; set; }

        public LibraryUsersModel LibraryUser { get; set; }
    }
}
