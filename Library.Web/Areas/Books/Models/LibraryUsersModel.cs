﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Web.Areas.Books.Models
{
    public class LibraryUsersModel : BaseModel
    {
        public int? BooksId { get; set; }

        public string UserId { get; set; }
    }
}
