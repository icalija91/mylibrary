﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Web.Automapper
{
    public class BAutoMapper : Profile
    {
        public BAutoMapper()
        {
            CreateMap<Library.Repository.Books, Library.Web.Areas.Books.Models.BooksModel>();
            CreateMap<Library.Web.Areas.Books.Models.BooksModel, Library.Repository.Books>();

            CreateMap<Library.Repository.LibraryUsers, Library.Web.Areas.Books.Models.LibraryUsersModel>();
            CreateMap<Library.Web.Areas.Books.Models.LibraryUsersModel, Library.Repository.LibraryUsers>();

            CreateMap<Library.Repository.Base, Library.Web.Areas.Books.Models.BaseModel>();
            CreateMap<Library.Web.Areas.Books.Models.BaseModel, Library.Repository.Base>();
        }
    }
}
