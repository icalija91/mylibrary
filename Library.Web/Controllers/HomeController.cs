﻿using AutoMapper;
using Library.Service.Services.Interfaces;
using Library.Web.Areas.Books.Models;
using Library.Web.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Library.Controllers
{

    [Authorize]
    public class HomeController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IBooksService booksService;
        private readonly IMapper mapper;

        public HomeController(IBooksService credentialsService, UserManager<ApplicationUser> userManager, IMapper mapper)
        {
            this.booksService = credentialsService;
            this.userManager = userManager;
            this.mapper = mapper;
        }


        public async Task<IActionResult> IndexAsync(string search = "")
        {
            var user = await userManager.GetUserAsync(User);
            var books = booksService.ReadAllByUserId(user.Id, search);

            return View(mapper.Map<List<BooksModel>>(books));
        }

        public IActionResult Privacy()
        {
            return View();
        }
    }
}