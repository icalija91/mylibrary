﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Web.Data
{
    public class Base
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity), Key()]
        public int Id { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        [Required]
        public DateTime DateUpdated { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(max)")]
        public string UserCreated { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(max)")]
        public string UserUpdated { get; set; }
    }
}
