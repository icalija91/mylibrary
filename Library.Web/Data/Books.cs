﻿using Library.Repository;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.Web.Data
{
    public class Books : Base
    {

        [Required]
        [Column(TypeName = "nvarchar(max)")]
        public string Name { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string Author { get; set; }

        [Column(TypeName = "int")]
        public int Year { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string Category { get; set; }

        [Column(TypeName = "int")]
        public int Rating { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string Description { get; set; }

        public ICollection<LibraryUsers> LibraryUsers { get; set; }
    }
}
