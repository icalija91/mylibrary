﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Web.Data
{
    public class LibraryUsers : Base
    {
        [ForeignKey("Books")]
        public int? BooksId { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(max)")]
        public string UserId { get; set; }
    }
}
