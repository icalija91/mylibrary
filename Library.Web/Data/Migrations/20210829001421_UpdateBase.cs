﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Library.Web.Data.Migrations
{
    public partial class UpdateBase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LibraryUsers_Books_BooksId",
                table: "LibraryUsers");

            migrationBuilder.DropIndex(
                name: "IX_LibraryUsers_BooksId",
                table: "LibraryUsers");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "LibraryUsers",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<int>(
                name: "BooksId",
                table: "LibraryUsers",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_LibraryUsers_BooksId",
                table: "LibraryUsers",
                column: "BooksId",
                unique: true,
                filter: "[BooksId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_LibraryUsers_Books_BooksId",
                table: "LibraryUsers",
                column: "BooksId",
                principalTable: "Books",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LibraryUsers_Books_BooksId",
                table: "LibraryUsers");

            migrationBuilder.DropIndex(
                name: "IX_LibraryUsers_BooksId",
                table: "LibraryUsers");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "LibraryUsers",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BooksId",
                table: "LibraryUsers",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LibraryUsers_BooksId",
                table: "LibraryUsers",
                column: "BooksId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_LibraryUsers_Books_BooksId",
                table: "LibraryUsers",
                column: "BooksId",
                principalTable: "Books",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
