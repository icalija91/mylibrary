﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Library.Web.Data.Migrations
{
    public partial class UpdateBase3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserCreated",
                table: "LibraryUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserUpdated",
                table: "LibraryUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCreated",
                table: "Books",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserUpdated",
                table: "Books",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserCreated",
                table: "LibraryUsers");

            migrationBuilder.DropColumn(
                name: "UserUpdated",
                table: "LibraryUsers");

            migrationBuilder.DropColumn(
                name: "UserCreated",
                table: "Books");

            migrationBuilder.DropColumn(
                name: "UserUpdated",
                table: "Books");
        }
    }
}
