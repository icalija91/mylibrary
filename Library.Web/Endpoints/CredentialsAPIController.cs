﻿using AutoMapper;
using Library.Service.Services.Interfaces;
using Library.Web.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Web.Endpoints
{
    [ApiController]
    public class CredentialsAPIController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IBooksService booksService;
        private readonly IMapper mapper;

        public CredentialsAPIController(IBooksService booksService, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IMapper mapper)
        {
            this.booksService = booksService;
            this.userManager = userManager;
            this.mapper = mapper;
            this.signInManager = signInManager;
        }
    }
}
